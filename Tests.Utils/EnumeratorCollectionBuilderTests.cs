﻿using System.Linq;
using System.Runtime.Serialization;
using Xunit;

namespace SeanDuffy.EnumGeneration.Business.Tests.Utils
{
    public class EnumeratorCollectionBuilderTests
    {
        [Fact]
        public void Build_Default_ReturnsOneEnumerator()
        {
            // Arrange
            const int expected = 1;

            // Act
            var enumeratorCollection = new EnumeratorCollectionBuilder().Build();
            
            // Assert
            var actual = enumeratorCollection.Count;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Build_MultipleEnumerators_ReturnsCorrectNumberOfEnumerators()
        {
            // Arrange
            const int expected = 4;

            // Act
            var enumeratorCollection = new EnumeratorCollectionBuilder()
                .NumberOfEnumerators(4)
                .Build();

            // Assert
            var actual = enumeratorCollection.Count;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Build_Default_EnumeratorHasCorrectName()
        {
            // Arrange
            const string expected = "Test1";

            // Act
            var enumeratorCollection = new EnumeratorCollectionBuilder().Build();

            // Assert
            var enumerator = enumeratorCollection.First();
            var actual = enumerator.Name;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Build_Default_EnumeratorHasCorrectNumberOfValues()
        {
            // Arrange
            const int expected = 3;

            // Act
            var enumeratorCollection = new EnumeratorCollectionBuilder().Build();

            // Assert
            var enumerator = enumeratorCollection.First();
            var enumeratorList = enumerator.GetEnumeratorListItems();
            var actual = enumeratorList.Count();
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Build_Default_EnumeratorListHasCorrectNaming()
        {
            // Arrange
            const string expected = "Test1Item1";

            // Act
            var enumeratorCollection = new EnumeratorCollectionBuilder().Build();

            // Assert
            var enumerator = enumeratorCollection.First();
            var enumeratorList = enumerator.GetEnumeratorListItems();
            var actual = enumeratorList.First().Name;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Build_Default_EnumeratorListHasCorrectValue()
        {
            // Arrange
            const int expected = 1;

            // Act
            var enumeratorCollection = new EnumeratorCollectionBuilder().Build();

            // Assert
            var enumerator = enumeratorCollection.First();
            var enumeratorList = enumerator.GetEnumeratorListItems();
            var actual = enumeratorList.First().Value;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Build_WithDataContract_DataContractAttributeExists()
        {
            // Arrange
            var expected = typeof (DataContractAttribute);

            // Act
            var enumeratorCollection = new EnumeratorCollectionBuilder()
                .WithDataContractAttribute()
                .Build();

            // Assert
            var enumerator = enumeratorCollection.First();
            var attribute = enumerator.GetEnumeratorAttributes().First();
            var actual = attribute.AttributeType;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Build_WithDataMember_DataMemberAttributeExists()
        {
            // Arrange
            var expected = typeof(DataMemberAttribute);

            // Act
            var enumeratorCollection = new EnumeratorCollectionBuilder()
                .WithDataMemberAttributes()
                .Build();

            // Assert
            var enumerator = enumeratorCollection.First();
            var enumeratorList = enumerator.GetEnumeratorListItems();
            var enumeratorListItem = enumeratorList.First();
            var attribute = enumeratorListItem.GetEnumeratorAttributes().First();
            var actual = attribute.AttributeType;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Build_WithDataMemberAndName_DataMemberAttributeExists()
        {
            // Arrange
            var expected = typeof(DataMemberAttribute);

            // Act
            var enumeratorCollection = new EnumeratorCollectionBuilder()
                .WithDataMemberAttributesIncludingName()
                .Build();

            // Assert
            var enumerator = enumeratorCollection.First();
            var enumeratorList = enumerator.GetEnumeratorListItems();
            var enumeratorListItem = enumeratorList.First();
            var attribute = enumeratorListItem.GetEnumeratorAttributes().First();
            var actual = attribute.AttributeType;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Build_WithDataMemberAndName_DataMemberAttributeHasNameProperty()
        {
            // Arrange
            const string expected = "Name";

            // Act
            var enumeratorCollection = new EnumeratorCollectionBuilder()
                .WithDataMemberAttributesIncludingName()
                .Build();

            // Assert
            var enumerator = enumeratorCollection.First();
            var enumeratorList = enumerator.GetEnumeratorListItems();
            var enumeratorListItem = enumeratorList.First();
            var attribute = enumeratorListItem.GetEnumeratorAttributes().First();
            var attributeProperties = attribute.GetProperties();
            var actual = attributeProperties.First().Name;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Build_WithDataMemberAndName_DataMemberAttributeHasNamePropertyValue()
        {
           // Arrange
           const string expected = "Test1Item1";

           // Act
           var enumeratorCollection = new EnumeratorCollectionBuilder()
               .WithDataMemberAttributesIncludingName()
               .Build();

           // Assert
           var enumerator = enumeratorCollection.First();
           var enumeratorList = enumerator.GetEnumeratorListItems();
           var enumeratorListItem = enumeratorList.First();
           var attribute = enumeratorListItem.GetEnumeratorAttributes().First();
           var attributeProperties = attribute.GetProperties();
           var attributeProperty = attributeProperties.First();
           var actual = attributeProperty.Value;
           Assert.Equal(expected, actual);
        }
    }
}