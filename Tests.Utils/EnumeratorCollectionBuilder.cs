﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using SeanDuffy.EnumGeneration.Domain.Building;


namespace SeanDuffy.EnumGeneration.Business.Tests.Utils
{
    public class EnumeratorCollectionBuilder
    {
        private readonly EnumeratorCollection _enumeratorCollection = new EnumeratorCollection();
        private int _numberOfEnumeratorsToCreate = 1;
        private bool _addDataContractToTypes;
        private bool _addDataMembersToEnumerationList;
        private bool _addDataMemberWithNameToEnumerationList;
        private const int NumberOfEnumeratorItemsToCreate = 3;

        /// <summary>
        /// How many Enumerators to create.
        /// </summary>
        /// <param name="numberOfEnumeratorsToCreate">Defaults to 1</param>
        /// <returns></returns>
        public EnumeratorCollectionBuilder NumberOfEnumerators(int numberOfEnumeratorsToCreate)
        {
            if (numberOfEnumeratorsToCreate < 1)
            {
                throw new EnumerationGeneratorException("At least one enumerator must be created by the test builder.");
            }

            _numberOfEnumeratorsToCreate = numberOfEnumeratorsToCreate;
            return this;
        }

        public EnumeratorCollectionBuilder WithDataContractAttribute()
        {
            _addDataContractToTypes = true;
            return this;
        }

        public EnumeratorCollectionBuilder WithDataMemberAttributes()
        {
            _addDataMembersToEnumerationList = true;
            return this;
        }

        public EnumeratorCollectionBuilder WithDataMemberAttributesIncludingName()
        {
            _addDataMemberWithNameToEnumerationList = true;
            return this;
        }

        public EnumeratorCollection Build()
        {
            for (var enumeratorCount = 1;
                 enumeratorCount < _numberOfEnumeratorsToCreate + 1;
                 enumeratorCount++)
            {
                var enumeratorName = string.Format("Test{0}", enumeratorCount);
                CreateEnumerator(enumeratorName);
            }

            return _enumeratorCollection;
        }

        private void CreateEnumerator(string enumeratorName)
        {
            var enumeratorTypeAttributes = new EnumeratorAttributeCollection();

            if (_addDataContractToTypes)
            {
                var dataContractAttribute = new EnumeratorAttribute(typeof (DataContractAttribute));
                enumeratorTypeAttributes.Add(dataContractAttribute);
            }

            var enumerator = new Enumerator(enumeratorName, enumeratorTypeAttributes);

            for (var enumeratorItemCount = 1;
                 enumeratorItemCount < NumberOfEnumeratorItemsToCreate + 1;
                 enumeratorItemCount++)
            {
               var enumeratorListItemAttributes = new EnumeratorAttributeCollection();
                var enumeratorListItemValue = enumeratorItemCount;
                var enumeratorListItemName = string.Format("{0}Item{1}", enumeratorName,
                                                           enumeratorItemCount);

                if (_addDataMemberWithNameToEnumerationList ||
                    _addDataMembersToEnumerationList)
                {
                    var properties = new List<EnumeratorAttributeProperty>();

                    if (_addDataMemberWithNameToEnumerationList)
                    {
                        var property = new EnumeratorAttributeProperty("Name", enumeratorListItemName, typeof(string));
                        properties.Add(property);
                    }

                    var dataMemberAttribute = new EnumeratorAttribute(
                        typeof(DataMemberAttribute), properties);

                    enumeratorListItemAttributes.Add(dataMemberAttribute);
                }

                enumerator.Add(
                    new EnumeratorListItem(enumeratorListItemValue,
                        enumeratorListItemName,
                        enumeratorListItemAttributes));
            }

            _enumeratorCollection.Add(enumerator);
        }
    }
}