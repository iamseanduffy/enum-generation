#Dynamic Enum Generation#

This is a set of libraries with a WinForms user interface that provides for the generation of an assembly of enumerations based upon database lookup tables.

The goal of this project is to ensure that code enumerations are kept in sync with database lookup values.