﻿using System;
using System.IO;
using SeanDuffy.EnumGeneration.Services.Building;
using SeanDuffy.EnumGeneration.Services.Packaging;
using Xunit;

namespace SeanDuffy.EnumGeneration.Services.Tests.Integration
{
    public class NugetPublisherTests : IDisposable
    {
        private const string TestSourceFileNameKey = @"readme.txt";
        private const string TestSourceFolderKey = @"test_source";
        private const string TestExportFolderKey = @"test_export";
        private const string TestExportFullPath = @"test_export\TestPackage.1.0.nupkg";

        public NugetPublisherTests()
        {
            CleanUpTestPackage();
            CreateTestPackageContents();
        }

        private void CreateTestPackageContents()
        {
            try
            {
                if (!Directory.Exists(TestSourceFolderKey))
                    Directory.CreateDirectory(TestSourceFolderKey);

                var fileFullPath = string.Format(@"{0}\{1}",
                                                 TestSourceFolderKey,
                                                 TestSourceFileNameKey);

                if (File.Exists(fileFullPath)) return;

                using (var fileStream = File.Create(fileFullPath))
                {
                    var streamWriter = new StreamWriter(fileStream);
                    streamWriter.Write("This is a temporary integration test file and is not required.");
                    streamWriter.Close();
                }
            }
            catch (Exception exception)
            {
                Assert.False(true, string.Format("There was a problem setting up the dummy package for the integration test : '{0}'", 
                    exception.Message));
            }
        }

        private void CleanUpTestPackage()
        {
            try
            {
                if (Directory.Exists(TestSourceFolderKey))
                {
                    var sourceFileFullPath = string.Format(@"{0}\{1}",
                                                           TestSourceFolderKey,
                                                           TestSourceFileNameKey);

                    if (File.Exists(sourceFileFullPath))
                        File.Delete(sourceFileFullPath);

                    Directory.Delete(TestSourceFolderKey);
                }

                if (!Directory.Exists(TestExportFolderKey)) return;


                if (File.Exists(TestExportFullPath))
                    File.Delete(TestExportFullPath);

                Directory.Delete(TestExportFolderKey);
            }
            catch (Exception exception)
            {
                Assert.False(true, string.Format("There was a problem cleaning up the dummy package for the integration test : '{0}'",
                    exception.Message));
            }
        }

        [Fact]
        public void Build_ValidConditionsAndMetaData_PackageCreated()
        {
            // Arrange
            var currentDirectory = Environment.CurrentDirectory;
            var sourcePath = string.Format(@"{0}\{1}",
                currentDirectory,
                TestSourceFolderKey);

            var exportPath = string.Format(@"{0}\{1}",
                currentDirectory,
                TestExportFolderKey);

            var version = new Version(1, 0);
            var fileService = new FileAndFolderService();
            var nugetPublisher = new NugetPublisherService(fileService);

            // Act
            nugetPublisher.BuildPackage(sourcePath,
                exportPath,
                "Test Author",
                "TestPackage",
                "Test Description",
                version);

            // Assert
            var errorMessage = GetErrorMessageForTestPackageRead();
            Assert.True(string.IsNullOrEmpty(errorMessage), errorMessage);
        }

        private string GetErrorMessageForTestPackageRead()
        {
            try
            {
                return File.Exists(TestExportFullPath)
                           ? string.Empty
                           : "Test export file was not created.";
            }
            catch (Exception exception)
            {
                var errorMessage = string.Format("Test export file was not created : '{0}'"
                                                 , exception.Message);
                return errorMessage;
            }
        }

        public void Dispose()
        {
            CleanUpTestPackage();
        }
    }
}