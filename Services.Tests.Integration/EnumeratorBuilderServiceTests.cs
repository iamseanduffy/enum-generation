﻿using System;
using System.IO;
using System.Reflection;
using System.Threading;
using SeanDuffy.EnumGeneration.Business.Tests.Utils;
using SeanDuffy.EnumGeneration.Services.Building;
using Xunit;

namespace SeanDuffy.EnumGeneration.Services.Tests.Integration
{
    public class EnumeratorBuilderServiceTests : IDisposable
    {
        public EnumeratorBuilderServiceTests()
        {
            CleanUpTestAssembly();
        }

        [Fact]
        public void CreateEnumerationLibrary_Revision_SavedCorrectly()
        {
            // Arrange
            var enumeratorCollection = new EnumeratorCollectionBuilder()
                .NumberOfEnumerators(2)
                .WithDataContractAttribute()
                .WithDataMemberAttributesIncludingName()
                .Build();

            var version = new Version(999, 999, 999, new Random().Next(99999));
            var expected = version.Revision;
            var assemblyFullPath = GetTestAssemblyFullPath();

            var enumeratorBuilder = new EnumeratorBuilderService();
            enumeratorBuilder.CreateEnumerationLibrary(enumeratorCollection,
                Environment.CurrentDirectory,
                "IntegrationTests",
                version);

            Thread.Sleep(10000);
            var enumerationAssembly = Assembly.LoadFrom(assemblyFullPath);

            // Act
            var assemblyName = enumerationAssembly.GetName();
            var actual = assemblyName.Version.Revision;

            // Assert
            Assert.Equal(expected, actual);
        }

        private string GetTestAssemblyFullPath()
        {
            var assemblyLocation = Environment.CurrentDirectory;
            const string assemblyNameString = "IntegrationTests";
            return string.Format(@"{0}\SeanDuffy.{1}.EnumeratedTypes.dll",
                assemblyLocation, assemblyNameString);
        }

        private void CleanUpTestAssembly()
        {
            var assemblyFullPath = GetTestAssemblyFullPath();

            if (File.Exists(assemblyFullPath))
            {
                File.Delete(assemblyFullPath);
            }
        }

        public void Dispose()
        {
            CleanUpTestAssembly();
        }
    }
}