﻿using SeanDuffy.EnumGeneration.Business.Tests.Utils;
using SeanDuffy.EnumGeneration.Domain.Building;
using SeanDuffy.EnumGeneration.Services.Building;
using Xunit;

namespace SeanDuffy.EnumGeneration.Services.Tests.Unit.Building
{
    public class EnumeratorBuilderTests
    {
        public EnumeratorBuilderTests()
        {
            SetUpValidEnumerationSources();
        }

        private EnumeratorCollection _validEnumeratorCollection;
        private EnumeratorBuilderService _enumerationBuilder;

        private void SetUpValidEnumerationSources()
        {
            _validEnumeratorCollection = new EnumeratorCollectionBuilder()
                .NumberOfEnumerators(2)
                .WithDataContractAttribute()
                .WithDataMemberAttributesIncludingName()
                .Build();
        }

        #region Constructor Tests

        [Fact]
        public void EnumerationBuilder_NullEnumeratorCollection_ThrowsEnumerationGeneratorException()
        {
            // Arrange
            _enumerationBuilder = new EnumeratorBuilderService();
            const string expected = "The EnumeratorCollection cannot be null or empty";

            // Act
            var actual = Assert.Throws<EnumerationGeneratorException>(() => _enumerationBuilder.CreateEnumerationLibrary(null, "AssemblyLocation")).Message;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EnumerationBuilder_EmptyEnumerationSources_ThrowsEnumerationGeneratorException()
        {
            // Arrange
            _enumerationBuilder = new EnumeratorBuilderService();
            const string expected = "The EnumeratorCollection cannot be null or empty";

            // Act
            var actual = Assert.Throws<EnumerationGeneratorException>(() => _enumerationBuilder.CreateEnumerationLibrary(new EnumeratorCollection(), "AssemblyLocation")).Message;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EnumerationBuilder_NoAssemblyLocation_ThrowsEnumerationGeneratorException()
        {
            // Arrange
            _enumerationBuilder = new EnumeratorBuilderService();
            const string expected = "AssemblyLocation cannot be empty";

            // Act
            var actual = Assert.Throws<EnumerationGeneratorException>(() => _enumerationBuilder.CreateEnumerationLibrary(_validEnumeratorCollection, string.Empty)).Message;

            // Assert
            Assert.Equal(expected, actual);
        }

        #endregion

        #region BuildEnumeratorTypeName Tests

        [Fact]
        public void BuildValidDescriptionString_InvalidDescription_ThrowsEnumerationGeneratorException()
        {
            // Arrange
            _enumerationBuilder = new EnumeratorBuilderService();
            const string expected = "EnumerationTypeName 'InvalidName!' is not valid.  A name must be supplied which contains only alphanumeric characters.";

            // Act
            var actual = Assert.Throws<EnumerationGeneratorException>(() => _enumerationBuilder.BuildEnumeratorTypeName("InvalidName!")).Message;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void BuildValidDescriptionString_EmptyDescription_EnumeGeneratorInvalidTypeNameException()
        {
            // Arrange
            _enumerationBuilder = new EnumeratorBuilderService();
            const string expected = "EnumerationTypeName not supplied.  A name must be supplied which contains only alphanumeric characters.";

            // Act
            var actual = Assert.Throws<EnumerationGeneratorException>(() => _enumerationBuilder.BuildEnumeratorTypeName(string.Empty)).Message;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void BuildValidDescriptionString_ValidDescriptionWithSpaces_DescriptionReturnedWithoutSpaces()
        {
            // Arrange
            const string expected = "TestEnumeration";
            _enumerationBuilder = new EnumeratorBuilderService();

            // Act
            var actual = _enumerationBuilder.BuildEnumeratorTypeName("Test Enumeration");

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void BuildValidDescriptionString_ValidDescription_DescriptionReturned()
        {
            // Arrange
            const string expected = "TestEnumeration";
            _enumerationBuilder = new EnumeratorBuilderService();

            // Act
            var actual = _enumerationBuilder.BuildEnumeratorTypeName("TestEnumeration");

            // Assert
            Assert.Equal(expected, actual);
        }

        #endregion


    }
}