﻿using SeanDuffy.EnumGeneration.Domain.Building;
using SeanDuffy.EnumGeneration.Services.Naming;
using Xunit;

namespace SeanDuffy.EnumGeneration.Services.Tests.Unit.Naming
{
    public class AssemblyNamerTests
    {
        [Fact]
        public void BuildCompleteAssemblyName_EmptyAssemblyName_DefaultReturned()
        {
            // Arrange
            const string expected = "SeanDuffy.EnumeratedTypes";
            var assemblyName = new AssemblyNamer();

            // Act
            var actual = assemblyName.CreateName(string.Empty, "EnumeratedTypes");

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void BuildCompleteAssemblyName_InvalidAssemblyName_ThrowsEnumerationGeneratorException()
        {
            // Arrange
            var assemblyName = new AssemblyNamer();
            const string expected = "Assembly name 'InvalidAssembly!' is invalid.  Only alphanumeric characters are allowed.";

            // Act
            var actual = Assert.Throws<EnumerationGeneratorException>(() => assemblyName.CreateName("InvalidAssembly!", "assemblyNameSuffix")).Message;
            
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void BuildCompleteAssemblyName_ValidAssemblyNameWithOutPeriod_FullValidReturned()
        {
            // Arrange
            const string expected = "SeanDuffy.TestAssembly.EnumeratedTypes";
            var assemblyName = new AssemblyNamer();

            // Act
            var actual = assemblyName.CreateName("TestAssembly", "EnumeratedTypes");

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void BuildCompleteAssemblyName_ValidAssemblyNameWithPeriod_FullValidReturned()
        {
            // Arrange
            const string expected = "SeanDuffy.TestAssembly.EnumeratedTypes";
            var assemblyName = new AssemblyNamer();

            // Act
            var actual = assemblyName.CreateName(".TestAssembly.", "EnumeratedTypes");

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
