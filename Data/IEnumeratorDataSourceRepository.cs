﻿using System.ComponentModel.Composition;
using SeanDuffy.EnumGeneration.Domain.Building;

namespace SeanDuffy.EnumGeneration.Data
{
    [InheritedExport]
    public interface IEnumeratorDataSourceRepository
    {
        EnumeratorCollection GetEnumeratorCollection();
    }
}