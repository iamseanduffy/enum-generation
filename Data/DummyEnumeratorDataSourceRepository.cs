﻿using System.Collections.Generic;
using SeanDuffy.EnumGeneration.Data.PartialClasses.Dummy;

namespace SeanDuffy.EnumGeneration.Data
{
    public class DummyEnumeratorDataSourceRepository : EnumeratorDataSourceRepositoryBase
    {
        private readonly List<CustomStatus> _customStatus = new List<CustomStatus>
            {
                new CustomStatus
                    {
                        CustomStatusId = 1,
                        Description = "CustomStatus1"
                    },
                new CustomStatus
                    {
                        CustomStatusId = 2,
                        Description = "CustomStatus2"
                    },
                new CustomStatus
                    {
                        CustomStatusId = 3,
                        Description = "CustomStatus3"
                    },
                new CustomStatus
                    {
                        CustomStatusId = 4,
                        Description = "CustomStatus4"
                    },
            };

        public override string ToString()
        {
            return "Mock Data Reader";
        }

        protected override Dictionary<string, IEnumerable<IEnumeratorListItemDataSource>>
            GetEnumeratorListItemDataSource()
        {
            var dataSourceDictionary = new Dictionary<string, IEnumerable<IEnumeratorListItemDataSource>>
                {
                    {"CustomStatus", _customStatus},
                };
            return dataSourceDictionary;
        }
    }
}