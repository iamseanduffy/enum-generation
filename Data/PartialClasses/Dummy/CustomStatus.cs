﻿namespace SeanDuffy.EnumGeneration.Data.PartialClasses.Dummy
{
    internal class CustomStatus : IEnumeratorListItemDataSource
    {
        public int CustomStatusId { get; set; }
        public string Description { get; set; }

        public string EnumeratorListItemDescription
        {
            get { return Description; }
        }

        public int EnumeratorListItemId
        {
            get { return CustomStatusId; }
        }
    }
}
