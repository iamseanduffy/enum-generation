﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using SeanDuffy.EnumGeneration.Domain.Building;

namespace SeanDuffy.EnumGeneration.Data
{
    public abstract class EnumeratorDataSourceRepositoryBase : IEnumeratorDataSourceRepository
    {
        protected abstract Dictionary<string, IEnumerable<IEnumeratorListItemDataSource>> GetEnumeratorListItemDataSource();

        public EnumeratorCollection GetEnumeratorCollection()
        {
            var enumeratorCollection = new EnumeratorCollection();
            var dataSourceDictionary = GetEnumeratorListItemDataSource();

            foreach (var dataSource in dataSourceDictionary)
            {
                if (dataSource.Value.Any() == false) continue;

                var enumerator = BuildEnumerator(dataSource.Value, dataSource.Key);
                enumeratorCollection.Add(enumerator);
            }

            return enumeratorCollection;
        }

        private Enumerator BuildEnumerator(IEnumerable<IEnumeratorListItemDataSource> enumeratorListItemDataSources,
                                            string enumeratorName)
        {
            var enumeratorAttribute = new EnumeratorAttribute(typeof(DataContractAttribute));
            var enumeratorAttributeProperty = new EnumeratorAttributeProperty("Name", enumeratorName, typeof(string));
            enumeratorAttribute.Add(enumeratorAttributeProperty);

            var enumerator = new Enumerator(enumeratorName);
            enumerator.Add(enumeratorAttribute);

            foreach (var enumerationListItemDataSource in enumeratorListItemDataSources)
            {
                var enumeratorListItem = BuildEnumeratorListItem(enumerationListItemDataSource.EnumeratorListItemId,
                                                                 enumerationListItemDataSource.
                                                                     EnumeratorListItemDescription);
                enumerator.Add(enumeratorListItem);
            }

            return enumerator;
        }

        private EnumeratorListItem BuildEnumeratorListItem(int enumeratorListItemId,
                                                             string enumeratorDescription)
        {
            var enumeratorListItem = new EnumeratorListItem(enumeratorListItemId, enumeratorDescription);
            var enumeratorListItemAttribute = new EnumeratorAttribute(typeof(DataContractAttribute));
            var enumeratorListItemAttributeProperty = new EnumeratorAttributeProperty("Name",
                                                                                      enumeratorDescription,
                                                                                      typeof(string));
            enumeratorListItemAttribute.Add(enumeratorListItemAttributeProperty);
            enumeratorListItem.Add(enumeratorListItemAttribute);
            return enumeratorListItem;
        }
    }
}
