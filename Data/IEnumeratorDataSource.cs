﻿namespace SeanDuffy.EnumGeneration.Data
{
    public interface IEnumeratorListItemDataSource
    {
        string EnumeratorListItemDescription { get; }
        int EnumeratorListItemId { get; }
    }
}
