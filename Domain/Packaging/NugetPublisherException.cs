﻿using System;

namespace SeanDuffy.EnumGeneration.Domain.Packaging
{
    public class NugetPublisherException : Exception
    {
       public NugetPublisherException(string message) :base(message){ }
       public NugetPublisherException(string message, Exception innerException) : base(message, innerException) { }
    }
}