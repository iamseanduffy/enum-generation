﻿using System;

namespace SeanDuffy.EnumGeneration.Domain.Building
{
    public class EnumerationGeneratorException : Exception
    {
       public EnumerationGeneratorException(string message) :base(message){ }
       public EnumerationGeneratorException(string message, Exception innerException) : base(message, innerException) { }
    }
}