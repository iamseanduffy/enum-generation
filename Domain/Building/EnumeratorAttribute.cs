﻿using System;
using System.Collections.Generic;

namespace SeanDuffy.EnumGeneration.Domain.Building
{
    public class EnumeratorAttribute
    {
        private readonly List<EnumeratorAttributeProperty> _properties;

        public EnumeratorAttribute(Type attributeType, List<EnumeratorAttributeProperty> properties = null)
        {
            if (attributeType.IsSubclassOf(typeof (Attribute)) == false)
            {
                throw new EnumerationGeneratorException("Type must be derived from Attribute when creating an Enumerator Attribute");
            }

            AttributeType = attributeType;
            _properties = properties ?? new List<EnumeratorAttributeProperty>();
        }

        public Type AttributeType { get; private set; }

        public void Add(EnumeratorAttributeProperty enumeratorAttributeProperty)
        {
            _properties.Add(enumeratorAttributeProperty);
        }

        public List<EnumeratorAttributeProperty> GetProperties()
        {
            return _properties;
        }
    }
}