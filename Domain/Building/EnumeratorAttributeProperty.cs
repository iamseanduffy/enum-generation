﻿using System;

namespace SeanDuffy.EnumGeneration.Domain.Building
{
    public class EnumeratorAttributeProperty
    {
        private readonly string _propertyName;
        private readonly string _propertyValue;
        private readonly Type _propertyValueType;

        public EnumeratorAttributeProperty(string propertyName, string propertyValue, Type propertyValueType)
        {
            if (propertyValueType != typeof (string) &&
                propertyValueType != typeof (bool) &&
                propertyValueType != typeof (int))
            {

                throw new EnumerationGeneratorException(
                    "Only string, bool and int are supported as Enumerator Attribute Property value types");
            }

            _propertyName = propertyName;
            _propertyValue = propertyValue;
            _propertyValueType = propertyValueType;
        }

        public string Name
        {
            get { return _propertyName; }
        }

        public Type ValueType
        {
            get { return _propertyValueType; }
        }

        public string Value
        {
            get { return _propertyValue; }
        }
    }
}