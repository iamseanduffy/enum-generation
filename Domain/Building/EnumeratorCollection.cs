﻿using System.Collections.Generic;
using System.Linq;

namespace SeanDuffy.EnumGeneration.Domain.Building
{
    public class EnumeratorCollection : List<Enumerator>
    {
        /// <summary>
        /// Add an enumerator to this collection
        /// </summary>
        /// <param name="enumerator"></param>
        public new void Add(Building.Enumerator enumerator)
        {
            if (this.Any(_ => _.Name == enumerator.Name))
                throw new EnumerationGeneratorException(
                    string.Format("EnumeratorCollection already contains an Enumeration '{0}'",
                        enumerator.Name));

            base.Add(enumerator);
        }
    }
}