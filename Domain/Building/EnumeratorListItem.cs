﻿
namespace SeanDuffy.EnumGeneration.Domain.Building
{
    public class EnumeratorListItem
    {
        public EnumeratorListItem(int value,
                                  string name,
                                  EnumeratorAttributeCollection enumeratorAttributes = null)
        {
            _enumeratorAttributes = enumeratorAttributes ?? new EnumeratorAttributeCollection();
            Value = value;
            Name = name;
        }

        public readonly int Value;
        public readonly string Name;
        private readonly EnumeratorAttributeCollection _enumeratorAttributes;

       public void Add(EnumeratorAttribute enumeratorAttribute)
       {
          _enumeratorAttributes.Add(enumeratorAttribute);
       }

        public EnumeratorAttributeCollection GetEnumeratorAttributes()
        {
            return _enumeratorAttributes;
        }
    }
}