﻿using System.Collections.Generic;
using System.Linq;

namespace SeanDuffy.EnumGeneration.Domain.Building
{
    public class Enumerator
    {
        public Enumerator(string name, EnumeratorAttributeCollection enumeratorAttributes = null)
        {
            Name = name;
            _enumeratorAttributes = enumeratorAttributes ?? new EnumeratorAttributeCollection();
        }

        public readonly string Name;
        private readonly EnumeratorAttributeCollection _enumeratorAttributes;
        private readonly List<EnumeratorListItem> _enumeratorListItems = new List<EnumeratorListItem>();

        public void Add(EnumeratorListItem enumeratorListItem)
        {
            if (_enumeratorListItems.Any(_ => _.Value == enumeratorListItem.Value))
            {
                throw new EnumerationGeneratorException(
                    string.Format("Enumerator already contains a value for '{0}'",
                        enumeratorListItem.Value));
            }

            if (_enumeratorListItems.Any(_ => _.Name == enumeratorListItem.Name))
            {
                throw new EnumerationGeneratorException(
                    string.Format("Enumerator already contains a name for '{0}'",
                        enumeratorListItem.Name));
            }

            _enumeratorListItems.Add(enumeratorListItem);
        }

        public void Add(EnumeratorAttribute enumeratorAttribute)
        {
            if (enumeratorAttribute == null)
            {
                throw new EnumerationGeneratorException("You cannot add null Enumerator Attribute to an Enumerator");
            }

            _enumeratorAttributes.Add(enumeratorAttribute);
        }

        public IEnumerable<EnumeratorListItem> GetEnumeratorListItems()
        {
            return _enumeratorListItems;
        }

        public EnumeratorAttributeCollection GetEnumeratorAttributes()
        {
            return _enumeratorAttributes;
        }
    }
}