﻿using System.Collections.Generic;
using System.Linq;

namespace SeanDuffy.EnumGeneration.Domain.Building
{
    public class EnumeratorAttributeCollection : List<EnumeratorAttribute>
    {
        public new void Add(EnumeratorAttribute enumerator)
        {
            if (this.Any(_ => _.AttributeType == enumerator.AttributeType))
            {
                throw new EnumerationGeneratorException(
                    string.Format("EnumeratorAttribute already contains an EnumeratorAttribute '{0}'",
                        enumerator.AttributeType));
            }

            base.Add(enumerator);
        }
    }
}