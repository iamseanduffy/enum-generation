﻿using System.Linq;
using SeanDuffy.EnumGeneration.Domain.Building;

namespace SeanDuffy.EnumGeneration.Services.Naming
{
    public class AssemblyNamer
    {
        public string CreateName(string partialAssemblyName, string assemblyNameSuffix)
        {
            if (string.IsNullOrEmpty(partialAssemblyName))
            {
                return GetDefaultAssemblyName(assemblyNameSuffix);
            }

            const string validCharacterArray = "abcdefghijklmnopqrstuvwxyz1234567890_.";

            if (partialAssemblyName.ToLower().Any(_ => validCharacterArray.Contains(_) == false))
            {
                throw new EnumerationGeneratorException
                    (string.Format("Assembly name '{0}' is invalid.  Only alphanumeric characters are allowed.",
                        partialAssemblyName));
            }

            var fullAssemblyName = GetFullAssemblyName(partialAssemblyName, assemblyNameSuffix);

            return fullAssemblyName;
        }

        public string CreateFileName(string partialAssemblyName, string assemblyNameSuffix)
        {
            var fullAssemblyName = CreateName(partialAssemblyName, assemblyNameSuffix);
            return fullAssemblyName + ".dll";
        }

        private string GetDefaultAssemblyName(string assemblyNameSuffix)
        {
            return "SeanDuffy." + assemblyNameSuffix;
        }

        private string GetFullAssemblyName(string partialAssemblyName, string assemblyNameSuffix)
        {
            partialAssemblyName = partialAssemblyName.Trim('.');
            assemblyNameSuffix = assemblyNameSuffix.Trim('.');

            return string.Format("SeanDuffy.{0}.{1}", partialAssemblyName, assemblyNameSuffix);
        }
    }
}
