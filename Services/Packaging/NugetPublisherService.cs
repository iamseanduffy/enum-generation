﻿using System;
using System.IO;
using System.Linq;
using NuGet;
using SeanDuffy.EnumGeneration.Domain.Packaging;
using SeanDuffy.EnumGeneration.Services.Building;

namespace SeanDuffy.EnumGeneration.Services.Packaging
{
    public class NugetPublisherService
    {
        private readonly IFileAndFolderService _fileAndFolderService;

        public NugetPublisherService(IFileAndFolderService fileAndFolderService)
        {
            _fileAndFolderService = fileAndFolderService;
        }

        public void BuildPackage(string sourcePath,
                                 string packageExportPath,
                                 ManifestMetadata manifestMetadata)
        {
            if (string.IsNullOrEmpty(sourcePath))
            {
                throw new NugetPublisherException("SourcePath cannot be empty when instantiating NugetPublisher");
            }

            if (string.IsNullOrEmpty(packageExportPath))
            {
                throw new NugetPublisherException("PackageExportPath cannot be empty when instantiating NugetPublisher");
            }

            if (manifestMetadata == null)
            {
                throw new NugetPublisherException("ManifestMetadata cannot be empty when instantiating NugetPublisher");
            }

            ValidateSourcePath(sourcePath);
            ValidatePath(packageExportPath);
            CreatePackageExport(manifestMetadata, packageExportPath);
            PopulateAndSavePackage(manifestMetadata, sourcePath, packageExportPath);
        }

        public void BuildPackage(string sourcePath,
                                 string packageExportPath,
                                 string author,
                                 string id,
                                 string description,
                                 Version version)
        {

            if (string.IsNullOrEmpty(author))
            {
                throw new NugetPublisherException("Author cannot be empty when instantiating NugetPublisher");
            }

            if (string.IsNullOrEmpty(id))
            {
                throw new NugetPublisherException("Id cannot be empty when instantiating NugetPublisher");
            }

            if (version == null)
            {
                throw new NugetPublisherException("Version cannot be null when instantiating NugetPublisher");
            }

            var manifestMetadata = new ManifestMetadata
                {
                    Authors = author,
                    Version = version.ToString(),
                    Id = id,
                    Description = description,
                };

            BuildPackage(sourcePath,
                         packageExportPath,
                         manifestMetadata);
        }

        internal string GetPackageFileName(ManifestMetadata manifestMetadata)
        {
            var fileName = string.Format("{0}.{1}.nupkg",
                                         manifestMetadata.Id,
                                         manifestMetadata.Version);

            return fileName.Replace(' ', '_');
        }

        internal string GetPackageFullPath(ManifestMetadata manifestMetadata, string packageExportPath)
        {
            var packageFileName = GetPackageFileName(manifestMetadata);
            return string.Format(@"{0}\{1}", packageExportPath, packageFileName);
        }


        private void ValidateSourcePath(string sourcePath)
        {
            ValidatePath(sourcePath);

            try
            {
                var sourceFiles = Directory.GetFiles(sourcePath);

                if (sourceFiles.Any() == false)
                {
                    throw new NugetPublisherException(string.Format("Source Path '{0}' does not contain any files",
                        sourcePath));
                }
            }
            catch (Exception exception)
            {
                throw new NugetPublisherException(string.Format("Unable to access Source Path '{0}'.", sourcePath),
                                                  exception);
            }
        }

        private void ValidatePath(string path)
        {
            if (path.IndexOfAny(Path.GetInvalidPathChars()) != -1)
            {
                throw new NugetPublisherException(string.Format("Path '{0}' contains invalid characters", path));
            }
        }

        private void CreatePackageExport(ManifestMetadata manifestMetadata, string packageExportPath)
        {

            var packageFullPath = GetPackageFullPath(manifestMetadata,
                                                     packageExportPath);

            try
            {
                _fileAndFolderService.CreateFolder(packageExportPath);
                _fileAndFolderService.CreateFile(packageFullPath);
            }
            catch (Exception exception)
            {
                var errorMessage = string.Format("Unable to create package file '{0}'.",
                                                 packageFullPath);
                throw new NugetPublisherException(errorMessage,
                                                  exception);
            }
        }

        internal void PopulateAndSavePackage(ManifestMetadata manifestMetadata,
                                             string sourcePath,
                                             string packageExportPath)
        {
            var packageBuilder = new PackageBuilder();

            var manifestFiles = new[]
                {
                    new ManifestFile
                        {
                            Source = "**"
                        }
                };

            try
            {
                packageBuilder.PopulateFiles(sourcePath, manifestFiles);
                packageBuilder.Populate(manifestMetadata);
                var packageFullPath = GetPackageFullPath(manifestMetadata,packageExportPath);
                using (var fileStream = _fileAndFolderService.GetStreamForFile(packageFullPath))
                    packageBuilder.Save(fileStream);
            }
            catch (Exception exception)
            {
                throw new NugetPublisherException("There was a problem building the package",
                                                  exception);
            }
        }
    }
}