﻿using System.Collections.Generic;
using SeanDuffy.EnumGeneration.Domain.Building;

namespace SeanDuffy.EnumGeneration.Services.Building
{
    public interface IEnumeratorCollectionFactory
    {
        IEnumerable<string> GetDataSourceNames();
        EnumeratorCollection CreateForDataSourceName(string dataSourceName);
    }
}
