﻿using System.IO;

namespace SeanDuffy.EnumGeneration.Services.Building
{
    public interface IFileAndFolderService
    {
        void CreateFolder(string folderPath);
        void CreateFile(string filePath);
        Stream GetStreamForFile(string filePath);
        bool DoesFileExist(string filePath);
        bool DoesFolderExist(string folderPath);
    }
}
