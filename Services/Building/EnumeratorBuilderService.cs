﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using SeanDuffy.EnumGeneration.Domain.Building;
using SeanDuffy.EnumGeneration.Services.Naming;
using SeanDuffy.EnumGeneration.Services.Versioning;

namespace SeanDuffy.EnumGeneration.Services.Building
{
    public sealed class EnumeratorBuilderService : IEnumerorBuilderService
    {
       // private const string DllExtension = ".dll";

        /// <summary>
        /// Enumeration Builder Service.  Responsible for the creation and persistence of an enumeration library.
        /// </summary>
        /// <param name="enumeratorCollection">The source of the enumerations.</param>
        /// <param name="assemblyLocation">The location on disk where the generated library will be persisted.</param>
        /// <param name="assemblyNameString">The name of the enumeration assembly which will be generated. Default value will be used if none supplied.</param>
        /// <param name="version">The version of the assembly to be generated.  If none supplied a 'simple' version based on the date and time of generation will be used.</param>
        public void CreateEnumerationLibrary(EnumeratorCollection enumeratorCollection,
            string assemblyLocation,
            string assemblyNameString = null,
            Version version = null)
        {
            if (enumeratorCollection == null ||
                enumeratorCollection.Any() == false)
            {
                throw new EnumerationGeneratorException("The EnumeratorCollection cannot be null or empty");
            }

            if (string.IsNullOrEmpty(assemblyLocation))
            {
                throw new EnumerationGeneratorException("AssemblyLocation cannot be empty");
            }

            var assemblyNamer = new AssemblyNamer();
            var nameForAssembly = assemblyNamer.CreateName(assemblyNameString, "EnumeratedTypes");
            var fileNameForAssembly = assemblyNamer.CreateFileName(assemblyNameString, "EnumeratedTypes");

            if (version == null)
            {
                var simpleDateTimeVersion = new SimpleDateTimeVersion();
                version = simpleDateTimeVersion.Create();
            }

            var assemblyName = new AssemblyName(nameForAssembly)
            {
                Version = version
            };

            var assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(
                assemblyName,
                AssemblyBuilderAccess.RunAndSave,
                assemblyLocation);

            var moduleBuilder = assemblyBuilder.DefineDynamicModule(assemblyName.Name, fileNameForAssembly);
            
            foreach (var enumerator in enumeratorCollection)
            {
                CreateEnumerator(assemblyBuilder, moduleBuilder, enumerator);
            }

            SaveDll(assemblyBuilder);
        }

        internal string BuildEnumeratorTypeName(string enumerationTypeName)
        {
            const string validCharacterArray = "abcdefghijklmnopqrstuvwxyz1234567890_ ";

            if (string.IsNullOrEmpty(enumerationTypeName) ||
                enumerationTypeName.ToLower().Any(_ => validCharacterArray.Contains(_) == false))
            {
                throw new EnumerationGeneratorException(
                    string.Format(
                        "EnumerationTypeName {0}.  A name must be supplied which contains only alphanumeric characters.",
                        string.IsNullOrEmpty(enumerationTypeName)
                            ? "not supplied"
                            : string.Format("'{0}' is not valid", enumerationTypeName)));
            }

            return RemoveInvalidCharactersFromEnumeratorName(enumerationTypeName);
        }

        internal string RemoveInvalidCharactersFromEnumeratorName(string enumerationString)
        {
            return enumerationString.Replace(" ", string.Empty).Replace("-", string.Empty);
        }

        internal void CreateEnumerator(AssemblyBuilder assemblyBuilder,
            ModuleBuilder moduleBuilder, 
            Enumerator enumerator)
        {
            var enumeratorAttributeCollection = enumerator.GetEnumeratorAttributes();
            var enumeratorTypeName = BuildEnumeratorTypeName(enumerator.Name);
            var enumBuilder = CreateEnumBuilderForEnumerationTypeName(assemblyBuilder, 
                moduleBuilder, 
                enumeratorTypeName);

            AddEnumeratorTypeAttributesToEnumBuilder(enumBuilder, enumeratorAttributeCollection);

            var enumeratorList = enumerator.GetEnumeratorListItems()
                                           .OrderBy(_ => _.Value);

            foreach (var enumeratorListItem in enumeratorList)
            {
                AddEnumeratorListItemToEnumBuilder(enumBuilder, enumeratorListItem);
            }

            enumBuilder.CreateType();
        }

        internal EnumBuilder CreateEnumBuilderForEnumerationTypeName(AssemblyBuilder assemblyBuilder,
            ModuleBuilder moduleBuilder,
            string name)
        {
            var assemblyName = assemblyBuilder.GetName();
            return moduleBuilder.DefineEnum(
                string.Format("{0}.{1}", assemblyName.Name, name), TypeAttributes.Public,
                typeof (int));
        }

        internal void AddEnumeratorTypeAttributesToEnumBuilder(EnumBuilder enumBuilder, EnumeratorAttributeCollection enumeratorAttributeCollection)
        {
            foreach (var enumeratorAttribute in enumeratorAttributeCollection)
            {
                var enumMemberAttributeBuilder = CreateCustomAttributeBuilderForAttribute(enumeratorAttribute);
                enumBuilder.SetCustomAttribute(enumMemberAttributeBuilder);
            }
        }

        internal CustomAttributeBuilder CreateCustomAttributeBuilderForAttribute(EnumeratorAttribute enumeratorAttribute)
        {
            var enumMemberAttribute = enumeratorAttribute.AttributeType.GetConstructor(new Type[] {});

            if (enumMemberAttribute == null)
            {
                throw new EnumerationGeneratorException(
                    string.Format("Unable to get constructor for EnumeratorAttribute '{0}'.",
                        enumeratorAttribute.AttributeType));
            }

            var namedProperties = new List<PropertyInfo>();
            var namedPropertyValues = new List<object>();
            var enumeratorAttributeProperties = enumeratorAttribute.GetProperties();

            foreach (var enumeratorAttributeProperty in enumeratorAttributeProperties)
            {
                var propertyInfo = enumeratorAttribute.AttributeType.GetProperty(enumeratorAttributeProperty.Name);

                if (propertyInfo == null)
                    throw new EnumerationGeneratorException(
                        string.Format("Unable to find property '{0}' in Enumerator Attribute type '{1}'.",
                                      enumeratorAttributeProperty.Name, enumeratorAttribute.AttributeType));

                namedProperties.Add(propertyInfo);

                if (enumeratorAttributeProperty.ValueType == typeof (string))
                {
                    namedPropertyValues.Add(enumeratorAttributeProperty.Value);
                }
                else if (enumeratorAttributeProperty.ValueType == typeof (bool))
                {
                    namedPropertyValues.Add(bool.Parse(enumeratorAttributeProperty.Value));
                }
                else if (enumeratorAttributeProperty.ValueType == typeof (int))
                {
                    namedPropertyValues.Add(int.Parse(enumeratorAttributeProperty.Value));
                }
            }

            var enumMemberAttributeBuilder = new CustomAttributeBuilder(
                enumMemberAttribute,
                new object[] {},
                namedProperties.ToArray(),
                namedPropertyValues.ToArray());

            return enumMemberAttributeBuilder;
        }

        internal void AddEnumeratorListItemToEnumBuilder(EnumBuilder enumBuilder, EnumeratorListItem enumeratorListItem)
        {
            var enumeratorAttributes = enumeratorListItem.GetEnumeratorAttributes();
            var enumeratorListItemName = RemoveInvalidCharactersFromEnumeratorName(enumeratorListItem.Name);
            var fieldBuilder = enumBuilder.DefineLiteral(enumeratorListItemName, enumeratorListItem.Value);

            AddEnumeratorAttributesToFieldBuilder(fieldBuilder, enumeratorAttributes);
        }

        internal void AddEnumeratorAttributesToFieldBuilder(FieldBuilder fieldBuilder,
                                                            IEnumerable<EnumeratorAttribute> enumeratorAttributes)
        {
            foreach (var enumeratorAttribute in enumeratorAttributes)
            {
                var enumMemberAttributeBuilder = CreateCustomAttributeBuilderForAttribute(enumeratorAttribute);
                fieldBuilder.SetCustomAttribute(enumMemberAttributeBuilder);
            }
        }

        internal void SaveDll(AssemblyBuilder assemblyBuilder)
        {
            var assemblyName = assemblyBuilder.GetName();

            try
            {
                assemblyBuilder.Save(string.Format("{0}{1}", assemblyName.Name, ".dll"));
            }
            catch (Exception exception)
            {
                throw new EnumerationGeneratorException("Error saving Enumeration dll.", exception);
            }
        }
    }
}