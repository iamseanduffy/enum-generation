﻿using System.IO;

namespace SeanDuffy.EnumGeneration.Services.Building
{
    public class FileAndFolderService : IFileAndFolderService
    {
        public void CreateFolder(string folderPath)
        {
            if (Directory.Exists(folderPath) == false)
            {
                Directory.CreateDirectory(folderPath);
            }
        }

        public void CreateFile(string filePath)
        {
            using (var fileStream = File.Create(filePath))
            {
                fileStream.Close();
            }
        }

        public Stream GetStreamForFile(string filePath)
        {
            return File.Open(filePath, FileMode.OpenOrCreate);
        }


        public bool DoesFileExist(string filePath)
        {
            return File.Exists(filePath);
        }

        public bool DoesFolderExist(string folderPath)
        {
            return Directory.Exists(folderPath);
        }
    }
}