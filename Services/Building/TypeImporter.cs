﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;

namespace SeanDuffy.EnumGeneration.Services.Building
{
    public class TypeImporter<T>
    {
        // ReSharper disable once UnassignedField.Compiler
        [ImportMany]
        private IEnumerable<T> _imports;

        public TypeImporter()
        {
            var catalog = new AggregateCatalog();
            catalog.Catalogs.Add(new DirectoryCatalog("."));
            var container = new CompositionContainer(catalog);
            container.ComposeParts(this);
        }

        public IEnumerable<T> GetImports()
        {
            return _imports;
        }
    }
}
