﻿using System.Collections.Generic;
using System.Linq;
using SeanDuffy.EnumGeneration.Data;
using SeanDuffy.EnumGeneration.Domain.Building;

namespace SeanDuffy.EnumGeneration.Services.Building
{
    public class EnumeratorCollectionFactory : IEnumeratorCollectionFactory
    {
        private readonly IEnumerable<IEnumeratorDataSourceRepository> _enumeratorDataSourceRepositories;

        public EnumeratorCollectionFactory()
        {
            var typeImporter = new TypeImporter<IEnumeratorDataSourceRepository>();
            _enumeratorDataSourceRepositories = typeImporter.GetImports();
        }

        public IEnumerable<string> GetDataSourceNames()
        {
            return _enumeratorDataSourceRepositories
                .Select(repository => repository.ToString())
                .ToList();
        }

        public EnumeratorCollection CreateForDataSourceName(string dataSourceName)
        {
            var enumeratorDataSourceRepository = _enumeratorDataSourceRepositories
                .FirstOrDefault(repository => repository.ToString() == dataSourceName);

            if (enumeratorDataSourceRepository == null)
            {
                throw new EnumerationGeneratorException(
                    string.Format("Unable to find Enumerator Data Source Repository '{0}'.",
                        dataSourceName));
            }

            return enumeratorDataSourceRepository.GetEnumeratorCollection();
        }
    }
}