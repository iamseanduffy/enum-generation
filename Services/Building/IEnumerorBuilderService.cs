﻿using System;
using SeanDuffy.EnumGeneration.Domain.Building;

namespace SeanDuffy.EnumGeneration.Services.Building
{
    public interface IEnumerorBuilderService
    {
        void CreateEnumerationLibrary(EnumeratorCollection enumeratorCollection,
            string assemblyLocation,
            string assemblyNameString = null,
            Version version = null);
    }
}