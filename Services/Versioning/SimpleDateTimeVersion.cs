﻿using System;

namespace SeanDuffy.EnumGeneration.Services.Versioning
{
    public class SimpleDateTimeVersion
    {
        public Version Create()
        {
            var versionRevisionString = int.Parse(string.Format("{0}{1}",
                DateTime.Now.Hour, DateTime.Now.Minute));

            return new Version(DateTime.Today.Year,
                DateTime.Today.Month,
                DateTime.Today.Day,
                versionRevisionString);
        }
    }
}