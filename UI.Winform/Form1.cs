﻿using System;
using System.Linq;
using System.Windows.Forms;
using SeanDuffy.Common.HistoryTracking;
using SeanDuffy.EnumGeneration.Domain.Building;
using SeanDuffy.EnumGeneration.Services.Building;
using SeanDuffy.EnumGeneration.Services.Naming;

namespace SeanDuffy.EnumGeneration.UI.Winform
{
    // ReSharper disable InconsistentNaming
    public partial class frmEnumGen : Form
        // ReSharper restore InconsistentNaming
    {
        private readonly IHistoryManager _assemblyHistory;
        private readonly IHistoryManager _namespaceHistory;
        private readonly IHistoryManager _locationHistory;
        private readonly IEnumeratorCollectionFactory _enumeratorCollectionFactory;
        private readonly IEnumerorBuilderService _enumerorBuilderService;
        private readonly IFileAndFolderService _fileAndFolderService;

        public frmEnumGen()
        {
            InitializeComponent();

            _assemblyHistory = HistoryFactory.GetInstance("Assembly");
            _namespaceHistory = HistoryFactory.GetInstance("Namespace");
            _locationHistory = HistoryFactory.GetInstance("Location");

            _enumeratorCollectionFactory = new EnumeratorCollectionFactory();
            _enumerorBuilderService = new EnumeratorBuilderService();
            _fileAndFolderService = new FileAndFolderService();

            BindComboBoxes();
            SetupGenerateButton();
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            if (ValidateInput())
            {
                var assemblyFileName = new AssemblyNamer().CreateFileName(cbAssembly.Text, ".dll");
                var assemblyFileNameWithPath = string.Format(@"{0}\{1}", cbAssemblyLocation.Text, assemblyFileName);

                if (_fileAndFolderService.DoesFileExist(assemblyFileNameWithPath))
                {
                    var dialogResult = MessageBox.Show(
                        "The enum library already exists in the chosen location.  Do you wish to overwrite?",
                        "Confirm",
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question);

                    if (dialogResult != DialogResult.Yes)
                    {
                        return;
                    }
                }

                lbOk.DataSource = null;
                CreateLibrary();
                UpdateHistory();
                var enumeratorCollection = _enumeratorCollectionFactory.CreateForDataSourceName(cbDataSource.Text);
                lbOk.DataSource = enumeratorCollection
                    .Select(_ => _.Name)
                    .ToList();
                BindComboBoxes();
            }
        }

        private void btnClearHistory_Click(object sender, EventArgs e)
        {
            _assemblyHistory.ClearHistory();
            _namespaceHistory.ClearHistory();
            _locationHistory.ClearHistory();

            cbAssembly.Text = string.Empty;
            cbAssemblyLocation.Text = string.Empty;

            cbAssembly.Focus();

            BindComboBoxes();
        }

        private void chkIsSimpleVersion_CheckedChanged(object sender, EventArgs e)
        {
            ToggleSimpleVersionOptions();
        }

        private void btnBrowseAssemblyLocation_Click(object sender, EventArgs e)
        {
            fbdAssemblyLocation.ShowDialog();
            if (!string.IsNullOrEmpty(fbdAssemblyLocation.SelectedPath))
                cbAssemblyLocation.Text = fbdAssemblyLocation.SelectedPath;
        }

        private void UpdateHistory()
        {
            _assemblyHistory.AddHistoryItem(cbAssembly.Text.Trim());
            _locationHistory.AddHistoryItem(cbAssemblyLocation.Text.Trim());
        }

        private void SetupGenerateButton()
        {
            btnGenerate.Enabled = cbDataSource.DataSource != null;
            btnGenerate.Text = btnGenerate.Enabled ? "Generate" : "Invalid Data Reader";
        }

        private void BindComboBoxes()
        {
            cbAssembly.DataSource = _assemblyHistory.GetHistory().ToList();
            cbAssemblyLocation.DataSource = _locationHistory.GetHistory().ToList();
            cbDataSource.DataSource = _enumeratorCollectionFactory.GetDataSourceNames().ToList();
        }

        private bool ValidateAndFocusOnFailure(MaskedTextBox maskedTextBox)
        {
            if (string.IsNullOrEmpty(maskedTextBox.Text))
            {
                maskedTextBox.Focus();
                return false;
            }

            return true;
        }

        private bool ValidateAndFocusOnFailure(ComboBox comboBox)
        {
            if (string.IsNullOrEmpty(comboBox.Text))
            {
                comboBox.Focus();
                return false;
            }

            return true;
        }


        private bool ValidateInput()
        {
            if ((chkIsSimpleVersion.Checked == false &&
                 (ValidateAndFocusOnFailure(txtVersionBuild) ||
                  ValidateAndFocusOnFailure(txtVersionRevision) ||
                  ValidateAndFocusOnFailure(txtVersionMajor) ||
                  ValidateAndFocusOnFailure(txtVersionMinor))) ||
                ValidateAndFocusOnFailure(cbAssembly) ||
                ValidateAndFocusOnFailure(cbAssemblyLocation) ||
                ValidateAndFocusOnFailure(cbDataSource))
            {
                MessageBox.Show("Ensure all fields are completed and valid.",
                    "Warning",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);

                return false;
            }

            Version version;

            if (chkIsSimpleVersion.Checked == false &&
                TryParseVersion(out version) == false)
            {
                MessageBox.Show("Ensure all version parameters are valid numbers.",
                    "Warning",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);

                return false;
            }

            if (_fileAndFolderService.DoesFolderExist(cbAssemblyLocation.Text) == false)
            {
                MessageBox.Show("Assembly location does not exist",
                    "Warning",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);

                return false;
            }

            return true;
        }

        private void ToggleSimpleVersionOptions()
        {
            lblVersionBuild.Enabled = !chkIsSimpleVersion.Checked;
            lblVersionMaj.Enabled = !chkIsSimpleVersion.Checked;
            lblVersionMinor.Enabled = !chkIsSimpleVersion.Checked;
            lblVersionRevision.Enabled = !chkIsSimpleVersion.Checked;
            txtVersionBuild.Enabled = !chkIsSimpleVersion.Checked;
            txtVersionRevision.Enabled = !chkIsSimpleVersion.Checked;
            txtVersionMajor.Enabled = !chkIsSimpleVersion.Checked;
            txtVersionMinor.Enabled = !chkIsSimpleVersion.Checked;
        }

        private void CreateLibrary()
        {
            Version version;

            if (TryParseVersion(out version) == false)
            {
                MessageBox.Show("Ensure all version parameters are valid numbers.",
                    "Warning",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }

            try
            {
                var enumeratorCollection = _enumeratorCollectionFactory.CreateForDataSourceName(cbDataSource.Text);

                if (enumeratorCollection == null ||
                    enumeratorCollection.Any() == false)
                {
                    MessageBox.Show("No Enumerations Returned from Enumeration Reader",
                        "Error",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }

                _enumerorBuilderService.CreateEnumerationLibrary(enumeratorCollection,
                    cbAssemblyLocation.Text.Trim(),
                    cbAssembly.Text.Trim(),
                    version);

                MessageBox.Show("Generation Completed Successfully",
                    "Info",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            catch (EnumerationGeneratorException enumerationGeneratorException)
            {
                MessageBox.Show(enumerationGeneratorException.Message,
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            catch (Exception exception)
            {
                MessageBox.Show(string.Format("Error Generating Enum ({0})",
                    exception.Message),
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private bool TryParseVersion(out Version version)
        {
            version = null;
            int major;
            int minor;
            int build;
            int revision;

            if (int.TryParse(txtVersionMajor.Text, out major) == false)
            {
                return false;
            }

            if (int.TryParse(txtVersionMinor.Text, out minor) == false)
            {
                return false;
            }

            if (int.TryParse(txtVersionBuild.Text, out build) == false)
            {
                return false;
            }

            if (int.TryParse(txtVersionRevision.Text, out revision) == false)
            {
                return false;
            }

            version = new Version(major, minor, build, revision);

            return true;
        }
    }
}