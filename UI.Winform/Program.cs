﻿using System;
using System.Windows.Forms;

namespace SeanDuffy.EnumGeneration.UI.Winform
{
   static class Program
   {
      /// <summary>
      /// The main entry point for the application.
      /// </summary>
      [STAThread]
      static void Main()
      {
         Application.EnableVisualStyles();
         Application.SetCompatibleTextRenderingDefault(false);
         Application.Run(new frmEnumGen());
         //Application.Run(new EnumWizard());
      }
   }
}
