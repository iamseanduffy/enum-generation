﻿namespace SeanDuffy.EnumGeneration.UI.Winform
{
   partial class frmEnumGen
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEnumGen));
            this.btnGenerate = new System.Windows.Forms.Button();
            this.lbOk = new System.Windows.Forms.ListBox();
            this.lbFail = new System.Windows.Forms.ListBox();
            this.lblWarnings = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblAssemblyName = new System.Windows.Forms.Label();
            this.grpVersion = new System.Windows.Forms.GroupBox();
            this.txtVersionRevision = new System.Windows.Forms.MaskedTextBox();
            this.txtVersionMinor = new System.Windows.Forms.MaskedTextBox();
            this.txtVersionBuild = new System.Windows.Forms.MaskedTextBox();
            this.txtVersionMajor = new System.Windows.Forms.MaskedTextBox();
            this.lblVersionRevision = new System.Windows.Forms.Label();
            this.lblVersionBuild = new System.Windows.Forms.Label();
            this.lblVersionMinor = new System.Windows.Forms.Label();
            this.lblVersionMaj = new System.Windows.Forms.Label();
            this.chkIsSimpleVersion = new System.Windows.Forms.CheckBox();
            this.btnClearHistory = new System.Windows.Forms.Button();
            this.cbAssembly = new System.Windows.Forms.ComboBox();
            this.grpAssemblyDetails = new System.Windows.Forms.GroupBox();
            this.btnBrowseAssemblyLocation = new System.Windows.Forms.Button();
            this.cbAssemblyLocation = new System.Windows.Forms.ComboBox();
            this.lblAssemblyLocation = new System.Windows.Forms.Label();
            this.grpResults = new System.Windows.Forms.GroupBox();
            this.grpGenerate = new System.Windows.Forms.GroupBox();
            this.cbDataSource = new System.Windows.Forms.ComboBox();
            this.lblDataReaderType = new System.Windows.Forms.Label();
            this.fbdAssemblyLocation = new System.Windows.Forms.FolderBrowserDialog();
            this.grpVersion.SuspendLayout();
            this.grpAssemblyDetails.SuspendLayout();
            this.grpResults.SuspendLayout();
            this.grpGenerate.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(324, 302);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(136, 21);
            this.btnGenerate.TabIndex = 11;
            this.btnGenerate.Text = "Generate Library";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // lbOk
            // 
            this.lbOk.FormattingEnabled = true;
            this.lbOk.Location = new System.Drawing.Point(9, 32);
            this.lbOk.Name = "lbOk";
            this.lbOk.Size = new System.Drawing.Size(439, 95);
            this.lbOk.TabIndex = 99;
            // 
            // lbFail
            // 
            this.lbFail.FormattingEnabled = true;
            this.lbFail.Location = new System.Drawing.Point(9, 146);
            this.lbFail.Name = "lbFail";
            this.lbFail.Size = new System.Drawing.Size(439, 95);
            this.lbFail.TabIndex = 99;
            // 
            // lblWarnings
            // 
            this.lblWarnings.AutoSize = true;
            this.lblWarnings.Location = new System.Drawing.Point(6, 130);
            this.lblWarnings.Name = "lblWarnings";
            this.lblWarnings.Size = new System.Drawing.Size(52, 13);
            this.lblWarnings.TabIndex = 3;
            this.lblWarnings.Text = "Warnings";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(145, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Successfully Imported Enums";
            // 
            // lblAssemblyName
            // 
            this.lblAssemblyName.AutoSize = true;
            this.lblAssemblyName.Location = new System.Drawing.Point(70, 31);
            this.lblAssemblyName.Name = "lblAssemblyName";
            this.lblAssemblyName.Size = new System.Drawing.Size(82, 13);
            this.lblAssemblyName.TabIndex = 6;
            this.lblAssemblyName.Text = "Assembly Name";
            // 
            // grpVersion
            // 
            this.grpVersion.Controls.Add(this.txtVersionRevision);
            this.grpVersion.Controls.Add(this.txtVersionMinor);
            this.grpVersion.Controls.Add(this.txtVersionBuild);
            this.grpVersion.Controls.Add(this.txtVersionMajor);
            this.grpVersion.Controls.Add(this.lblVersionRevision);
            this.grpVersion.Controls.Add(this.lblVersionBuild);
            this.grpVersion.Controls.Add(this.lblVersionMinor);
            this.grpVersion.Controls.Add(this.lblVersionMaj);
            this.grpVersion.Controls.Add(this.chkIsSimpleVersion);
            this.grpVersion.Location = new System.Drawing.Point(12, 134);
            this.grpVersion.Name = "grpVersion";
            this.grpVersion.Size = new System.Drawing.Size(454, 100);
            this.grpVersion.TabIndex = 4;
            this.grpVersion.TabStop = false;
            this.grpVersion.Text = "Version Details";
            // 
            // txtVersionRevision
            // 
            this.txtVersionRevision.Enabled = false;
            this.txtVersionRevision.Location = new System.Drawing.Point(363, 67);
            this.txtVersionRevision.Mask = "00";
            this.txtVersionRevision.Name = "txtVersionRevision";
            this.txtVersionRevision.PromptChar = ' ';
            this.txtVersionRevision.Size = new System.Drawing.Size(30, 20);
            this.txtVersionRevision.TabIndex = 9;
            this.txtVersionRevision.Text = "0";
            // 
            // txtVersionMinor
            // 
            this.txtVersionMinor.Enabled = false;
            this.txtVersionMinor.Location = new System.Drawing.Point(363, 47);
            this.txtVersionMinor.Mask = "00";
            this.txtVersionMinor.Name = "txtVersionMinor";
            this.txtVersionMinor.PromptChar = ' ';
            this.txtVersionMinor.Size = new System.Drawing.Size(30, 20);
            this.txtVersionMinor.TabIndex = 7;
            this.txtVersionMinor.Text = "0";
            // 
            // txtVersionBuild
            // 
            this.txtVersionBuild.Enabled = false;
            this.txtVersionBuild.Location = new System.Drawing.Point(249, 67);
            this.txtVersionBuild.Mask = "00";
            this.txtVersionBuild.Name = "txtVersionBuild";
            this.txtVersionBuild.PromptChar = ' ';
            this.txtVersionBuild.Size = new System.Drawing.Size(30, 20);
            this.txtVersionBuild.TabIndex = 8;
            this.txtVersionBuild.Text = "0";
            // 
            // txtVersionMajor
            // 
            this.txtVersionMajor.Enabled = false;
            this.txtVersionMajor.Location = new System.Drawing.Point(249, 47);
            this.txtVersionMajor.Mask = "00";
            this.txtVersionMajor.Name = "txtVersionMajor";
            this.txtVersionMajor.PromptChar = ' ';
            this.txtVersionMajor.Size = new System.Drawing.Size(30, 20);
            this.txtVersionMajor.TabIndex = 6;
            this.txtVersionMajor.Text = "0";
            // 
            // lblVersionRevision
            // 
            this.lblVersionRevision.AutoSize = true;
            this.lblVersionRevision.Enabled = false;
            this.lblVersionRevision.Location = new System.Drawing.Point(286, 70);
            this.lblVersionRevision.Name = "lblVersionRevision";
            this.lblVersionRevision.Size = new System.Drawing.Size(48, 13);
            this.lblVersionRevision.TabIndex = 15;
            this.lblVersionRevision.Text = "Revision";
            // 
            // lblVersionBuild
            // 
            this.lblVersionBuild.AutoSize = true;
            this.lblVersionBuild.Enabled = false;
            this.lblVersionBuild.Location = new System.Drawing.Point(172, 70);
            this.lblVersionBuild.Name = "lblVersionBuild";
            this.lblVersionBuild.Size = new System.Drawing.Size(30, 13);
            this.lblVersionBuild.TabIndex = 13;
            this.lblVersionBuild.Text = "Build";
            // 
            // lblVersionMinor
            // 
            this.lblVersionMinor.AutoSize = true;
            this.lblVersionMinor.Enabled = false;
            this.lblVersionMinor.Location = new System.Drawing.Point(286, 50);
            this.lblVersionMinor.Name = "lblVersionMinor";
            this.lblVersionMinor.Size = new System.Drawing.Size(71, 13);
            this.lblVersionMinor.TabIndex = 11;
            this.lblVersionMinor.Text = "Minor Version";
            // 
            // lblVersionMaj
            // 
            this.lblVersionMaj.AutoSize = true;
            this.lblVersionMaj.Enabled = false;
            this.lblVersionMaj.Location = new System.Drawing.Point(172, 50);
            this.lblVersionMaj.Name = "lblVersionMaj";
            this.lblVersionMaj.Size = new System.Drawing.Size(71, 13);
            this.lblVersionMaj.TabIndex = 8;
            this.lblVersionMaj.Text = "Major Version";
            // 
            // chkIsSimpleVersion
            // 
            this.chkIsSimpleVersion.AutoSize = true;
            this.chkIsSimpleVersion.Checked = true;
            this.chkIsSimpleVersion.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIsSimpleVersion.Location = new System.Drawing.Point(6, 19);
            this.chkIsSimpleVersion.Name = "chkIsSimpleVersion";
            this.chkIsSimpleVersion.Size = new System.Drawing.Size(237, 17);
            this.chkIsSimpleVersion.TabIndex = 5;
            this.chkIsSimpleVersion.Text = "Use Simple Version (based on date and time)";
            this.chkIsSimpleVersion.UseVisualStyleBackColor = true;
            this.chkIsSimpleVersion.CheckedChanged += new System.EventHandler(this.chkIsSimpleVersion_CheckedChanged);
            // 
            // btnClearHistory
            // 
            this.btnClearHistory.Location = new System.Drawing.Point(359, 82);
            this.btnClearHistory.Name = "btnClearHistory";
            this.btnClearHistory.Size = new System.Drawing.Size(75, 23);
            this.btnClearHistory.TabIndex = 4;
            this.btnClearHistory.Text = "Clear History";
            this.btnClearHistory.UseVisualStyleBackColor = true;
            this.btnClearHistory.Click += new System.EventHandler(this.btnClearHistory_Click);
            // 
            // cbAssembly
            // 
            this.cbAssembly.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbAssembly.FormattingEnabled = true;
            this.cbAssembly.Location = new System.Drawing.Point(158, 28);
            this.cbAssembly.MaxDropDownItems = 9;
            this.cbAssembly.Name = "cbAssembly";
            this.cbAssembly.Size = new System.Drawing.Size(276, 21);
            this.cbAssembly.TabIndex = 1;
            // 
            // grpAssemblyDetails
            // 
            this.grpAssemblyDetails.Controls.Add(this.btnBrowseAssemblyLocation);
            this.grpAssemblyDetails.Controls.Add(this.cbAssemblyLocation);
            this.grpAssemblyDetails.Controls.Add(this.lblAssemblyLocation);
            this.grpAssemblyDetails.Controls.Add(this.lblAssemblyName);
            this.grpAssemblyDetails.Controls.Add(this.btnClearHistory);
            this.grpAssemblyDetails.Controls.Add(this.cbAssembly);
            this.grpAssemblyDetails.Location = new System.Drawing.Point(12, 12);
            this.grpAssemblyDetails.Name = "grpAssemblyDetails";
            this.grpAssemblyDetails.Size = new System.Drawing.Size(454, 116);
            this.grpAssemblyDetails.TabIndex = 0;
            this.grpAssemblyDetails.TabStop = false;
            this.grpAssemblyDetails.Text = "Assembly Details";
            // 
            // btnBrowseAssemblyLocation
            // 
            this.btnBrowseAssemblyLocation.Location = new System.Drawing.Point(158, 82);
            this.btnBrowseAssemblyLocation.Name = "btnBrowseAssemblyLocation";
            this.btnBrowseAssemblyLocation.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseAssemblyLocation.TabIndex = 3;
            this.btnBrowseAssemblyLocation.Text = "Browse";
            this.btnBrowseAssemblyLocation.UseVisualStyleBackColor = true;
            this.btnBrowseAssemblyLocation.Click += new System.EventHandler(this.btnBrowseAssemblyLocation_Click);
            // 
            // cbAssemblyLocation
            // 
            this.cbAssemblyLocation.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbAssemblyLocation.FormattingEnabled = true;
            this.cbAssemblyLocation.Location = new System.Drawing.Point(158, 55);
            this.cbAssemblyLocation.MaxDropDownItems = 9;
            this.cbAssemblyLocation.Name = "cbAssemblyLocation";
            this.cbAssemblyLocation.Size = new System.Drawing.Size(276, 21);
            this.cbAssemblyLocation.TabIndex = 2;
            // 
            // lblAssemblyLocation
            // 
            this.lblAssemblyLocation.AutoSize = true;
            this.lblAssemblyLocation.Location = new System.Drawing.Point(57, 58);
            this.lblAssemblyLocation.Name = "lblAssemblyLocation";
            this.lblAssemblyLocation.Size = new System.Drawing.Size(95, 13);
            this.lblAssemblyLocation.TabIndex = 8;
            this.lblAssemblyLocation.Text = "Assembly Location";
            // 
            // grpResults
            // 
            this.grpResults.Controls.Add(this.label2);
            this.grpResults.Controls.Add(this.lbOk);
            this.grpResults.Controls.Add(this.lbFail);
            this.grpResults.Controls.Add(this.lblWarnings);
            this.grpResults.Location = new System.Drawing.Point(12, 328);
            this.grpResults.Name = "grpResults";
            this.grpResults.Size = new System.Drawing.Size(454, 246);
            this.grpResults.TabIndex = 103;
            this.grpResults.TabStop = false;
            this.grpResults.Text = "Results";
            // 
            // grpGenerate
            // 
            this.grpGenerate.Controls.Add(this.cbDataSource);
            this.grpGenerate.Controls.Add(this.lblDataReaderType);
            this.grpGenerate.Location = new System.Drawing.Point(14, 240);
            this.grpGenerate.Name = "grpGenerate";
            this.grpGenerate.Size = new System.Drawing.Size(452, 56);
            this.grpGenerate.TabIndex = 10;
            this.grpGenerate.TabStop = false;
            this.grpGenerate.Text = "Data Sources";
            // 
            // cbDataSource
            // 
            this.cbDataSource.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbDataSource.FormattingEnabled = true;
            this.cbDataSource.Location = new System.Drawing.Point(156, 19);
            this.cbDataSource.MaxDropDownItems = 9;
            this.cbDataSource.Name = "cbDataSource";
            this.cbDataSource.Size = new System.Drawing.Size(276, 21);
            this.cbDataSource.TabIndex = 10;
            // 
            // lblDataReaderType
            // 
            this.lblDataReaderType.AutoSize = true;
            this.lblDataReaderType.Location = new System.Drawing.Point(55, 22);
            this.lblDataReaderType.Name = "lblDataReaderType";
            this.lblDataReaderType.Size = new System.Drawing.Size(95, 13);
            this.lblDataReaderType.TabIndex = 12;
            this.lblDataReaderType.Text = "Data Reader Type";
            // 
            // fbdAssemblyLocation
            // 
            this.fbdAssemblyLocation.Description = "Please select where you would like to save the Assembly";
            // 
            // frmEnumGen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(476, 582);
            this.Controls.Add(this.grpGenerate);
            this.Controls.Add(this.grpResults);
            this.Controls.Add(this.grpAssemblyDetails);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.grpVersion);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmEnumGen";
            this.Text = "Dynamic Enum Generator";
            this.grpVersion.ResumeLayout(false);
            this.grpVersion.PerformLayout();
            this.grpAssemblyDetails.ResumeLayout(false);
            this.grpAssemblyDetails.PerformLayout();
            this.grpResults.ResumeLayout(false);
            this.grpResults.PerformLayout();
            this.grpGenerate.ResumeLayout(false);
            this.grpGenerate.PerformLayout();
            this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.Button btnGenerate;
      private System.Windows.Forms.ListBox lbOk;
      private System.Windows.Forms.ListBox lbFail;
      private System.Windows.Forms.Label lblWarnings;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Label lblAssemblyName;
      private System.Windows.Forms.GroupBox grpVersion;
      private System.Windows.Forms.CheckBox chkIsSimpleVersion;
      private System.Windows.Forms.Label lblVersionMinor;
      private System.Windows.Forms.Label lblVersionMaj;
      private System.Windows.Forms.Label lblVersionRevision;
      private System.Windows.Forms.Label lblVersionBuild;
      private System.Windows.Forms.MaskedTextBox txtVersionRevision;
      private System.Windows.Forms.MaskedTextBox txtVersionMinor;
      private System.Windows.Forms.MaskedTextBox txtVersionBuild;
      private System.Windows.Forms.MaskedTextBox txtVersionMajor;
      private System.Windows.Forms.ComboBox cbAssembly;
      private System.Windows.Forms.Button btnClearHistory;
      private System.Windows.Forms.GroupBox grpAssemblyDetails;
      private System.Windows.Forms.GroupBox grpResults;
      private System.Windows.Forms.GroupBox grpGenerate;
      private System.Windows.Forms.Label lblDataReaderType;
      private System.Windows.Forms.ComboBox cbAssemblyLocation;
      private System.Windows.Forms.Label lblAssemblyLocation;
      private System.Windows.Forms.FolderBrowserDialog fbdAssemblyLocation;
      private System.Windows.Forms.Button btnBrowseAssemblyLocation;
      private System.Windows.Forms.ComboBox cbDataSource;
   }
}

